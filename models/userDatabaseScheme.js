const mongoose = require('mongoose')

const Schema = mongoose.Schema

const userSchema = new Schema({
    userId: {
        type: Number,
        required: true,
        unique: true,
    },
    messageAuthor: {
        type: String,
        required: true,
    },
    postCount: Number,
    adminFlag: Boolean,
    timestamp: Date, 
    updated_at: Date,
})

const User = mongoose.model('User', userSchema)

module.exports = User
