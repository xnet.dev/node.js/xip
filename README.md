# x!p-bot

A funny chatty and feature rich bot. Also a little security guard.


# Installation / Usage

You need node.js >= 8.x & npm being installed.

After pull, run "npm install" in the polly-folder.

When npm is done, you need to add your Discord-Bot Token to the
  ./config.json

Finally you can start x!p with
  node xip.js


# Discord Integration

https://discordapp.com/developers/applications/me

For any help on getting App/Bot running on Discord-platform itself, hit up their support pages.

Otherwise, you can just try to create a app and after success looking for the bot creation in the app down the page.

# WARNING

Be sure to keep your secret tokens safe! Leaking would be bad for your server.
