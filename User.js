class ReqUser {
  constructor(userId, messageAuthor) {
    this.userId = userId;
    this.messageAuthor = messageAuthor;
  }
}

module.exports = ReqUser;
