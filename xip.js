// // x!p config
const { discordToken, mongodbHost, mongodbPort, commandPrefix  } = require('./config.json');

// // Discord API
const Discord = require('discord.js');
const client = new Discord.Client();

// // User registration handling
const reqUser = require('./User.js');
const userArray = [];

// // TODO connect to mongodb with username / pw + remote
// // MongoDB
const mongoose = require('mongoose')
const userModel = require('./models/userDatabaseScheme')
mongoose.connect(`mongodb://${mongodbHost}:${mongodbPort}/xip`, {
  connectTimeoutMS: 3000
  // Note that mongoose will **not** pull `bufferCommands` from the query string
});

// // Array to store the users io. to register
// const axios = require('axios');
// const fs = require('fs');



// Client on ready
client.on('ready', () => {
  console.log(`Logged in and ready! -> ${client.user.tag}`);
});

//   ____ _   _    _  _____
//  / ___| | | |  / \|_   _|
// | |   | |_| | / _ \ | |
// | |___|  _  |/ ___ \| |
//  \____|_| |_/_/   \_\_|
//

// // Client on message
client.on('message', message => {
    analyseChat(message);
    console.log("------END OF MESSAGE TASK ------")
});

function analyseChat (message) {

    // // Get ID from user, who send the last message
    const userId = message.author.id
    const messageAuthor = message.author.username

    checkCommands(message, userId, messageAuthor);
    handleMessage(userId, messageAuthor)

}



//   ____ __  __ ____  ____
//  / ___|  \/  |  _ \/ ___|
// | |   | |\/| | | | \___ \
// | |___| |  | | |_| |___) |
//  \____|_|  |_|____/|____/
//
function checkCommands (message, userId, messageAuthor) {

    // // cut to prefix
    if (!message.content.startsWith(commandPrefix) || message.author.bot) return;
    const args = message.content.slice(commandPrefix.length).split(/ +/);
    const command = args.shift().toLowerCase();
    // const userId = message.author.id

    switch (command) {
        case 'server':
            message.channel.send(`Server name: ${message.guild.name}\nTotal members: ${message.guild.memberCount}`);
            break;

        case 'user-info':
            message.channel.send(`Your username: ${message.author.username}\nYour ID: ${message.author.id}`);
            break;

        case 'avatar':
            showAvatar (message);
            break;

        case 'rank':
            showRank (message, userId);
            break;


        case 'register':
          register(message, userId, messageAuthor);
            break;

        // case 'getip':
        //     getCurrentIp (message);
        //     break;

        default:
            message.channel.send(`No command defined`)
    }
}



//  _____ ______     __
// | ____|  _ \ \   / /
// |  _| | | | \ \ / /
// | |___| |_| |\ V /
// |_____|____/  \_/
//
// // Create new array/object for queue
function register (message, userId, messageAuthor) {

  let isNotRegistered = true
  for (var i = 0; i < userArray.length; i++) {
    if ( userArray[i].userId == userId ) {
      isNotRegistered = false
    }
  }

  if (isNotRegistered) {
    userArray.push(new reqUser(userId, messageAuthor));
    message.channel.send('Registered!')
  }

  else {
    message.channel.send('You are already registered.')
  }

}

// // Database input
function handleMessage (userId, messageAuthor) {
    // // check if user exists
    userModel.find({
        userId: userId
    }, function(err, user) {

        if (err) throw err;

        // // if user found
        if (user.length == 1) {
            // // increse postcounter
            const flag = 1 // -1 for decrease
            userModel.findOneAndUpdate({
                userId: userId
            }, {
                $inc: {
                    postCount: flag
                }
            }, function(err, user) {
                if (err) throw err;
            })
        }
        else {
            // // create new user
            const newUser = new userModel({
                userId: userId,
                messageAuthor: messageAuthor,
                postCount: 1,
                adminFlag: false,
            })
            // // save new user
            newUser.save(function(err) {
                if (err) throw err
            })
        }
    })

}

function showRank (message, userId) {

    userModel.find({
        // // get specific user
        userId: userId
    }, function(err, user) {
        if (err) throw err;
        // // object of user
        if ( user[0] == null ) {
            const hqthink  = client.emojis.get("445810870996566036")
            message.channel.send(`Who are you? Never spoke to you before. ${hqthink}`)
        }
        else {
          message.channel.send('You have ' + parseInt(user[0].postCount + 1) + ' messages on record.')
        }
    })

}


function showAvatar (message) {

    if (!message.mentions.users.size) {
        // return message.channel.send(`Your avatar: ${message.author.displayAvatarURL}`);
        return message.reply(`Tell me what avatar you wanna see.`);
    }

    const avatarList = message.mentions.users.map(user => {
        return `${user.username} > ${user.displayAvatarURL}`;
    });

    message.channel.send(avatarList);
}


// function getCurrentIp (message) {
//
//  axios.get('https://api.ipify.org/?format=json')
//     .then(function (response) {
//         console.log(response.data.ip)
//         message.channel.send(response.data.ip)
//         saveData(response.data.ip, "output.json")
//     })
//     .catch(function (error) {
//         console.log(error);
//     })
// }
//
// function saveData (data, filename) {
//
//     fs.writeFile(`./${filename}`, data, function(err) {
//         if(err) {
//             return console.log(err);
//         }
//
//         // message.channel.send("The file was saved!");
//     });
// }

console.log("EOF -----------------------------")
client.login(discordToken);
